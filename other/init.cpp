#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual ~Engine() = default;
};

long data_gen()
{
    static auto seed = 0L;
    return ++seed;
}

class Data
{
    int data_{ data_gen() };
    string password_ = "unknown";
public:
    Data() = default;
    
    Data(int data) : data_{data}
    {
    }

    Data(double) = delete;

    Data(const Data&) = default;
    Data& operator=(const Data&) = default;

    int data() const
    {
        return data_;
    }

    string password() const
    {
        return password_;
    }
};

class Nocopyable
{
    int* data_;
public:
    Nocopyable(int* data) : data_{data}
    {}

    ~Nocopyable()
    {
        delete data_;
    }

    Nocopyable(const Nocopyable&) = delete;
    Nocopyable& operator=(const Nocopyable&) = delete;
};

int main()
{
    Data d1;    
    cout << d1.data() << " - " << d1.password() << endl;

    Data d2;
    cout << d2.data() << " - " << d2.password() << endl;

    Data d3 = d1;
    d2 = d1;

    Nocopyable nc1{ new int(10)};
    // Nocopyable nc2 = nc1;
}