#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 1a - wyświetl parzyste
    // TODO

    // 1b - wyswietl ile jest parzystych
    // TODO

    int eliminators[] = { 3, 5, 7 };
    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    // TODO

    // 3 - tranformacja: podnieś liczby do kwadratu
    // TODO

    // 4 - wypisz 5 najwiekszych liczb
    // TODO

    // 5 - policz wartosc srednia
    // TODO

    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    // TODO
}
