#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <set>
#include <map>
#include <functional>
#include <chrono>

using namespace std;

class Lambda_1
{
public:
	void operator()() const
	{
		cout << "simple lambda!" << endl;
	}
};

enum class Gender { male, female };

class Person
{
	int id_;
	string name_;
	Gender gender_;
public:
	Person(int id, const string& name, Gender gender) : id_{id}, name_{name}, gender_{gender}
	{}

	int id() const
	{
		return id_;
	}

	string name() const
	{
		return name_;
	}

	Gender gender() const
	{
		return gender_;
	}
};

void print_desc(const Person& p, string(*f)(const Person&))
{
	cout << "Description: ";
	f(p);
}

class Lambda_2
{
	const Gender wanted;
	int& counter;
public:
	Lambda_2(Gender wanted, int counter) : wanted{wanted}, counter{counter}
	{}

	void operator()(const Person& p) const { if (p.gender() == wanted) counter++; }
};

class Scale
{
public:
	// The constructor.
	explicit Scale(int scale) : scale_(scale) {}

	// Prints the product of each element in a vector object
	// and the scale value to the console.
	void apply_scale(const vector<int>& v) const
	{
		for_each(v.begin(), v.end(), [this](int n) { cout << n * scale_ << endl; });
	}

private:
	int scale_;
};

auto vector_generator()
{
	vector<int> vec_seed = { 1, 2, 3, 4, 5 };

	return [vec_seed]() mutable
		{
			for (auto& item : vec_seed)
				++item;

			return vec_seed;
	};
}

void print_vec(const vector<int>& v, string msg)
{
	cout << "v: ";
	for (const auto& item : v)
		cout << item << " ";
	cout << endl;
}

//class Kilometer
//{
//	int km_;
//public:
//	Kilometer(int km) : km_{km}
//	{}
//	
//	constexpr Kilometer operator""km(int km)
//	{
//		return Kilometer{ km };
//	}
//};

int main()
{
	//auto = 100km;

	auto interval = chrono::milliseconds(100);

	int billion = 1'000'000'000;

	auto l1 = []() { cout << "simple lambda!" << endl; };
	l1();

	auto l_explained = Lambda_1{};
	l_explained();

	vector<Person> people = { {1, "Jan", Gender::male}, {8, "Adam", Gender::male}, {3, "Grze�", Gender::male}, {10, "Zenon", Gender::female}, {9, "Ewa", Gender::female} };

	sort(people.begin(), people.end(), [](const auto& p1, const auto& p2) { return p1.name() < p2.name(); });

	cout << "Sorted by name: ";
	for (const auto& p : people)
		cout << p.name() << " ";
	cout << endl;

	sort(people.begin(), people.end(), 
		[](const Person& p1, const Person& p2)
		{
			return p1.id() < p2.id();
		});

	cout << "Sorted by id: ";
	for (const auto& p : people)
		cout << p.name() << " ";
	cout << endl;

	vector<string> desc;

	auto get_person_desc = [](const Person& p) -> string
	{
		if (p.gender() == Gender::male)
			return p.name() + " - Male";
		else
			return (p.name() + " - Female").c_str();
	};

	transform(people.begin(), people.end(), back_inserter(desc), get_person_desc);

	cout << "desc: ";
	for (const auto& d : desc)
		cout << d << "; ";
	cout << endl;

	print_desc(people[0], get_person_desc);

	Gender wanted = Gender::female;
	int counter = 0;

	for_each(people.begin(), people.end(), [wanted, &counter](const Person& p) { if (p.gender() == wanted) counter++; });


	cout << "Female count: " << counter << endl;
	cout << "Male count: " << count_if(people.begin(), people.end(), [](const Person& p) { return p.gender() == Gender::male; }) << endl;

	auto comp_by_id = [](const Person& p1, const Person& p2) { return p1.id() < p2.id(); };

	set<Person, decltype(comp_by_id)> set_people(comp_by_id);

	for (const auto& p : people)
		set_people.insert(p);

	cout << "Set of people: ";
	cout << "Sorted by id: ";
	for (const auto& p : people)
		cout << p.id() << " - " << p.name() << "; ";
	cout << endl;

	map<string, function<bool(const Person& p1, const Person& p2)> > predicates = 
									 { {"by_name", [](const Person& p1, const Person& p2) { return p1.name() < p2.name(); }},
									   {"by_id", [](const Person& p1, const Person& p2) { return p1.id() < p2.id(); } },
									   { "by_name_desc", [](const Person& p1, const Person& p2) { return p1.name() > p2.name(); } } };

	sort(people.begin(), people.end(), predicates["by_name_desc"]);

	cout << "Sorted by name desc: ";
	for (const auto& p : people)
		cout << p.name() << " ";
	cout << endl;

	int seed = 10;
	auto l_mutable = [seed]() mutable { seed++; cout << seed << endl; };

	auto my_gen = vector_generator();

	auto v1 = my_gen();
	auto v2 = my_gen();
	auto v3 = my_gen();

	print_vec(v1, "v1");
	print_vec(v2, "v2");
	print_vec(v3, "v3");

	auto new_gen = vector_generator();

	print_vec(new_gen(), "new gen");

	system("PAUSE");
}	