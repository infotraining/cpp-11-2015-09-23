#include <iostream>
#include <string>
#include <memory>

using namespace std;

template <typename... Base>
class Derived : public Base...
{
};

class Base1
{};

class Base2
{};

template <typename T>
using Owner = unique_ptr<T>;

template <typename T, typename... Arg>
Owner<T> make_owner(Arg&&... arg)
{
	return Owner<T>{new T(forward<Arg>(arg)...)};
}

void print()
{
	cout << endl;
}

template <typename T, typename... Tail>
void print(const T& arg1, const Tail&... params)
{
	cout << arg1 << " ";
	print(params...); // expension pack
}

template <typename... T>
constexpr size_t get_size_of_parameter_pack(const T&...)
{
	return sizeof...(T);
}

int main()
{
	Derived<Base1, Base2> d1;

	Owner<string> ptr_str1 = make_owner<string>();

	Owner<string> ptr_str2 = make_owner<string>("Hello");

	print(1, 4.33, "Hello", string("World"));

	cout << "Wielkosc paczki: " << get_size_of_parameter_pack(1, 2, 3, 4, 5) << endl;

	system("PAUSE");
}
