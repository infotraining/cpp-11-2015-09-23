#include <iostream>
#include <memory>

using namespace std;

void foo(int* ptr)
{
	if (ptr)
		cout << "foo(int*: " << *ptr << ")" << endl;
	else
		cout << "foo(int*: nullptr)" << endl;
}

void foo(nullptr_t)
{
	cout << "foo(nullptr_t)" << endl;
}

void foo(int value)
{
	cout << "foo(int: " << value << ")" << endl;
}

template <typename Arg>
void forward_with_pointer(Arg arg)
{
	foo(arg);
}

int main()
{
	int x = 10;
	int* ptr1 = nullptr;
	int* ptr2{}; // int* ptr2 = nullptr;

	foo(ptr1);

	foo(nullptr);

	forward_with_pointer(nullptr);

	shared_ptr<int> sptr1 = nullptr;
	shared_ptr<int> sptr2{ nullptr };
	shared_ptr<int> sptr3;

	if (sptr1 == nullptr)
	{
		cout << "sptr1 is nullptr" << endl;
	}

	system("PAUSE");
}