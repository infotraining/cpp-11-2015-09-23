#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <cstring>

using namespace std;

class String
{
	char* text_;
public:
	String(const char* text) 
	{
		cout << "String ctor(" << text << ")" << endl;

		auto size_of_string = strlen(text);
		text_ = new char[size_of_string + 1];

		strcpy(text_, text);
	}

	String(const String& other) : text_{new char[strlen(other.data())  + 1]}
	{
		cout << "String copy ctor(" << other.data() << ")" << endl;
		strcpy(text_, other.data());
	}

	String& operator=(const String& other)
	{
		cout << "String copy operator=(" << other.data() << ")" << endl;

		if (this != &other)
		{
			char* temp = new char[strlen(other.data()) + 1];
			strcpy(temp, other.data());

			delete[] text_;
			text_ = temp;
		}

		return *this;
	}

	String(String&& other) noexcept : text_{other.text_}
	{
		cout << "String move ctor(" << text_ << ")" << endl;
		other.text_ = nullptr;
	}

	String& operator=(String&& other) noexcept
	{
		cout << "String move operator=(" << other.data() << ")" << endl;

		if (this != &other)
		{
			delete[] text_;
			text_ = other.text_;
			other.text_ = nullptr;
		}

		return *this;
	}

	~String()
	{
		cout << "String dtor()" << endl;
		delete[] text_;
	}

	const char* data() const
	{
		return text_;
	}

	String operator+(const String& other) const
	{
		string temp = string(data()) + string(other.data());
		String result{ temp.c_str() };

		return result;
	}
};

ostream& operator<<(ostream& out, const String& str)
{
	out << str.data();
	return out;
}

String get_full_name(const String& name)
{
	String result = String("Mr.") + name;
	return result;
}

class NoCopyableData // implicitly nocopyable
{
	vector<int> vec_ = { 1, 2, 3, 4, 5 };
	int* buffer_;
	size_t size_;
public:
	NoCopyableData(size_t size, int value) : buffer_{ new int[size] }, size_{ size }
	{
		fill(buffer_, buffer_ + size_, value);
	}

	~NoCopyableData()
	{
		delete[] buffer_;
	}

	NoCopyableData(NoCopyableData&& other) : vec_{ move(other.vec_) }, buffer_{ other.buffer_ }, size_{ other.size_ }
	{
		other.buffer_ = nullptr;
		other.size_ = 0;
	}

	NoCopyableData& operator=(NoCopyableData&& other)
	{
		if (this != &other)
		{
			vec_ = move(other.vec_);

			buffer_ = other.buffer_;
			size_ = other.size_;

			other.buffer_ = nullptr;
			other.size_ = 0;
		}

		return *this;
	}
};

class Data
{
public:
	vector<int> data1_ {1, 2, 3, 4};
	string name_ = "Data";
	shared_ptr<int> data2_;
	NoCopyableData nc_data_{100, 13};
	
	Data(int data = 0) : data2_{ make_shared<int>(data) }
	{}

	Data(const Data&) = delete;
	Data& operator=(const Data&) = delete;

	Data(Data&&) = default;
	Data& operator=(Data&&) = default;

	~Data()
	{
		cout << "log: ~Data()" << endl;
	}

	void print() const
	{
		cout << "data1: ";
		for (const auto& item : data1_)
			cout << item << " ";
		cout << "\nname: " << name_;
		cout << "\ndata2: " << *data2_ << endl;
	}
};

static_assert(is_move_constructible<Data>::value, "Data nie ma konstruktora przenoszącego");
static_assert(is_move_assignable<Data>::value, "Data nie ma op= przenoszącego");


int main()
{
	String name = "John";

	cout << name << endl;

	String name2 = get_full_name(name);
	cout << name2 << endl;

	vector<String> names;

	names.push_back(name);
	names.push_back(move(name));

	// resusing name
	name = String("Ted");
	cout << name << endl;

	names.push_back(get_full_name(name));

	cout << "names: ";
	for(const auto& name : names)
	{
		cout << name << " ";
	}
	cout << endl;

	{
		Data d1{ 13 };

		Data d2 = move(d1);

		cout << "po move: size " << d1.data1_.size() << "; name: " << d1.name_ << endl;

		NoCopyableData ed1{ 100, 1 };
		NoCopyableData ed2 = move(ed1);
	}

	cout << "\n\n";
	{
		vector<String> vec;

		vec.push_back(String("t1"));
		cout << "\n";
		vec.push_back(String("t2"));
		cout << "\n";
		vec.push_back(String("t3"));
		cout << "\n";
		vec.push_back(String("t4"));
		cout << "\n";
		vec.push_back(String("t5"));
		cout << "\n";
		vec.push_back(String("t6"));
		cout << "\n";
		vec.push_back(String("t7"));
		cout << "\n";
		vec.push_back(String("t8"));
		cout << "\n";
		vec.push_back(String("t9"));
	}

	system("PAUSE");
}