#include <iostream>
#include <type_traits>

using namespace std;

enum class Coffee : uint8_t {espresso = 1, latte = 2, cappucino = 3 };

int main()
{
	Coffee c1 = Coffee::cappucino;

	c1 = static_cast<Coffee>(2);

	typedef underlying_type<Coffee>::type IndexType;

	IndexType index = static_cast<IndexType>(Coffee::latte);
}

