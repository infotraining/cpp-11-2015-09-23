#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
	const vector<int> vec = { 1, 2, 3, 4, 5 };

	cout << "vec: ";
	for(auto item : vec)
	{
		cout << item << " ";
	}
	cout << endl;

	for (auto it = vec.begin(); it != vec.end(); ++it)
	{
		auto item = *it;
		cout << item << " ";
	}
	cout << endl;

	vector<string> words = { "one", "two", "three" };
	
	cout << "words: ";
	for(const auto& w : words)
	{
		cout << w << " ";
	}
	cout << endl;

	for (auto it = words.begin(); it != words.end(); ++it)
	{
		const auto& item = *it;
		cout << item << " ";
	}
	cout << endl;

	int tab[10] = { 1, 2, 3, 4 };

	cout << "tab: ";
	for(const auto& item : tab)
	{
		cout << item << " ";
	}
	cout << endl;

	vector<int> evens;
	copy_if(begin(tab), end(tab), back_inserter(evens), [](int x) { return x % 2 == 0; });

	for(const auto& item : { 1, 2, 3, 4, 5, 6 })
	{
		cout << item << " ";
	}

	system("PAUSE");
}