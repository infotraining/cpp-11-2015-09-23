#include <iostream>
#include <string>
#include <set>
#include <functional>
#include <vector>

using namespace std;

void foo(const long* ptr)
{
	auto ptr_var = const_cast<long*>(ptr);
	(*ptr_var)++;
}

template <typename T>
void deduce_type(T& item)
{
	
}

int main()
{
	const auto i = 42L;

	//foo(&i); // ub

	cout << i << endl;

	auto pi = 3.14;

	auto& ref_pi = pi;

	vector<int> numbers = { 1, 6, 7, 34, 2, 33, 8 };

	for (auto it = numbers.cbegin(); it != numbers.cend(); ++it)
	{
		cout << *it << " ";
	}
	cout << endl;

	deduce_type(42);

	const long& ref = i;

	deduce_type(ref);

	auto j = ref;
	auto& ref_j = ref;

	const double _2pi = 6.28;

	auto temp = _2pi;

	const vector<int> data = { 1, 2, 3, 4 };
	const vector<int>& ref_data = data;

	auto auto_data = ref_data;
	auto& auto_ref_data = ref_data;

	const int tab[10] = { 1, 2, 3, 4 };

	deduce_type(tab);

	auto auto_tab = tab;
	auto& auto_ref_tab = tab;

	auto auto_foo = foo;
	auto& auto_ref_foo = foo;

	auto&& universal_ref = 42;

	system("PAUSE");
}