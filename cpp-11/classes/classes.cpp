#include <iostream>
#include <string>
#include <vector>
#include <memory>

using namespace std;

class Engine
{
public:
	virtual void start() const { cout << "Engine starts" << endl; };
	virtual void stop() const { cout << "Engine stops" << endl; };
	virtual ~Engine() = default;
};

class Diesel : public Engine
{
	void start() const override { cout << "Diesel starts" << endl; };
	void stop() const override { cout << "Diesel stops" << endl; };
};

long data_gen()
{
	static auto seed = 0L;
	return ++seed;
}

class Data
{
	long data_{ data_gen() };
	string password_ = "unknown";
public:
	Data() = default;
	
	Data(int data) : Data{data, "unknown"}
	{
		cout << "End of Data(int: " << data_ << ")" << endl;
		//throw runtime_error("Error");
	}

	Data(int data, const string& password) : data_{data}, password_{password}
	{
		cout << "End of Data(int: " << data_ << ", string: " << password_ << ")" << endl;
	}

	Data(double) = delete;

	Data(const Data&) = default;
	Data& operator=(const Data&) = default;

	virtual ~Data()
	{
		cout << "~Data(int: " << data_ << ", string: " << password_ << ")" << endl;			 
	}

	int data() const
	{
		return data_;
	}

	string password() const
	{
		return password_;
	}

	virtual void calculate()
	{
		cout << "Calculate data: " << data_ << endl;
	}
};

class SuperData final : public Data
{
	int extra_data_{};
public:
	using Data::Data;

	virtual void calculate()
	{
		cout << "Calculate super data: " << data() << endl;
	}
};

//class HyperData : public SuperData
//{
//	
//};

class Nocopyable
{
	int* data_;
public:
	Nocopyable(int* data) : data_{data}
	{}

	~Nocopyable()
	{
		delete data_;
	}

	Nocopyable(const Nocopyable&) = delete;
	Nocopyable& operator=(const Nocopyable&) = delete;
};

int main()
{
	Data d1;	
	cout << d1.data() << " - " << d1.password() << endl;

	Data d2;
	cout << d2.data() << " - " << d2.password() << endl;

	Data d3 = d1;
	d2 = d1;

	Nocopyable nc1{ new int(10)};
	// Nocopyable nc2 = nc1;

	try
	{
		Data d4{ 13 };
	}
	catch(...)
	{
		cout << "Wyjatek" << endl;
	}

	SuperData sd1{ 1, "string" };
	SuperData sd2{ 2 };
	SuperData sd3{};

	sd1.calculate();
	sd2.calculate();
	sd3.calculate();

	shared_ptr<Engine> engine{ new Diesel() };

	engine->start();
	engine->stop();

	system("PAUSE");
}