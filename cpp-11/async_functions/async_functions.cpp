#include <iostream>
#include <future>
#include <vector>
#include <thread>
#include <random>
#include <string>

using namespace std;

string download(string url)
{
	cout << "Start downloading " << url << endl;

	random_device rd;
	mt19937_64 gen(rd());
	uniform_int_distribution<> dist(1, 10);

	auto interval = dist(gen);
	this_thread::sleep_for(chrono::seconds(interval));

	if (interval % 2 == 0)
		throw runtime_error("Error#13 for " + url);

	return "Content of " + url;
}

int main()
{
	future<string> file_content1 = async(launch::async, [] { return download("text.txt"); });
	auto file_content2 = async(launch::deferred, [] { return download("file.dat"); });

	auto files = { "f1.dat", "f2.dat", "f3.dat" };

	vector<future<string>> future_files;

	for (const auto& f : files)
		future_files.push_back(async(launch::async, [f]() { return download(f); }));

	future_files.push_back(move(file_content1));
	future_files.push_back(move(file_content2));


	for (auto& ff : future_files)
	{
		try
		{
			cout << ff.get() << endl;
		}
		catch(const runtime_error& e)
		{
			cout << "Exception: " << e.what() << endl;
		}
	}

	system("PAUSE");
}