#include <iostream>
#include <map>
#include <string>

using namespace std;

template <typename T>
auto multiply(const T& a, const T& b)// -> decltype(a * b)
{
	return a * b;
}

auto foo(int x)
{
	if (x % 2 == 0)
		return x * 2.0;
	return static_cast<double>(x);
}

auto find_item(map<int, string>& m, int key) -> decltype(m.at(key))
{
	return m.at(key);
}

int main()
{
	map<int, string> numbers = { {1, "one"}, {2, "two"} };

	auto m1 = numbers; // kopiowanie

	decltype(numbers) m2; // pusta mapa - konstruktor domy�lny

	decltype(numbers)::value_type my_pair = { 6, "six" };

	auto& item = m2[9];
	item = "nine";

	decltype(m2[9]) item_ref = m2[9];
	item_ref = "nine";

	cout << find_item(numbers, 2) << endl;

	find_item(numbers, 1) = "jeden";

	cout << numbers[1] << endl;

	system("PAUSE");
}