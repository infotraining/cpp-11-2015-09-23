#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>
#include <set>

using namespace std;

struct Data
{
	string fname;
	string lname;
	string city;

	bool operator==(const Data& d) const
	{
		return tie(lname, fname, city) == tie(d.lname, d.fname, d.city);
	}

	bool operator<(const Data& d) const
	{
		return tie(lname, fname, city) < tie(d.lname, d.fname, d.city);
	}

};

tuple<int, int, double> calc_stat(const vector<int>& vec)
{
	auto minmax = minmax_element(vec.begin(), vec.end());
	auto avg = accumulate(vec.begin(), vec.end(), 0.0) / vec.size();

	return make_tuple(*minmax.first, *minmax.second, avg);
}

int main()
{
	vector<int> data = { 65, 234, 62456, 3, 534, 33, 6456245, -234, 3454, 34645 };

	auto stat_result = calc_stat(data);

	cout << "min: " << get<0>(stat_result) << endl;
	cout << "max: " << get<1>(stat_result) << endl;
	cout << "avg: " << get<double>(stat_result) << endl;

	int min, max;
	//double avg;

	//tuple<int&, int&, double&> ref_stat{ min, max, avg };

	tie(min, max, ignore) = calc_stat(data);

	cout << "min: " << min << endl;
	cout << "max: " << max << endl;
	//cout << "avg: " << avg << endl;

	set<int> numbers = { 1, 5, 7 };

	decltype(numbers.begin()) where;
	bool was_inserted;

	tie(where, was_inserted) = numbers.insert(5);

	if (was_inserted)
		cout << "Wstawiono " << *where << endl;
	else
		cout << *where << " juz jest w zbiorze" << endl;

	system("PAUSE");
}
