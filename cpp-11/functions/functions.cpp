#include <iostream>
#include <string>
#include <functional>

using namespace std;

string format(int x, const string& prefix)
{
	return prefix + ": " + to_string(x);
}

class Formatter
{
	string special_prefix_;
public:
	Formatter(const string& special_prefix) : special_prefix_{special_prefix}
	{}

	string operator()(int x, const string& prefix)
	{
		return special_prefix_ + " - " + prefix + ": " + to_string(x);
	}
};

class Service
{
	function<void(string)> on_srv_start_;

public:
	void register_handler_on_start(function<void(string)> handler)
	{
		on_srv_start_ = handler;
	}

	void start()
	{
		if (on_srv_start_)
			on_srv_start_("Service starts...");
	}
};

class Logger
{
public:
	void log(const string& message)
	{
		cout << "Log: " << message << endl;
	}
};

void log_it(const string& message)
{
	cout << "logging: " << message << endl;
}

int main()
{
	string txt = "12";
	
	int value = stoi(txt);

	cout << format(value, "value") << endl;

	function<string(int, const string&)> f;

	try
	{
		f(2, "val");
	}
	catch(const bad_function_call& e)
	{
		cout << "Exception: " << e.what() << endl;
	}

	f = &format;  // 1

	cout << f(2, "called by function - val") << endl;

	f = Formatter{ "Formatter" }; // 2

	cout << f(2, "called by functor - val") << endl;

	string lambda_prefix = "Lambda";

	f = [&lambda_prefix](int x, const string& prefix) { return lambda_prefix + " - " + prefix + ": " + to_string(x); };

	cout << f(2, "called by lambda - val") << endl;


	Logger logger;
	
	Service srv;

	srv.register_handler_on_start([&logger](string msg) { logger.log(msg); });

	srv.start();

	srv.register_handler_on_start(&log_it);

	srv.start();

	system("PAUSE");
}