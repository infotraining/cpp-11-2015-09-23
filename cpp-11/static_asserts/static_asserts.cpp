#include <iostream>
#include <type_traits>

using namespace std;

template <typename T>
void compute(const T& value)
{
	static_assert(is_arithmetic<T>::value, "T nie jest typem liczbowym");

	cout << "Obliczenia dla " << value << endl;
}

template <typename T, size_t N>
void fill_buffer(T (&arr)[N])
{
	static_assert(N <= 255, "N jest > 255");

	cout << "Wypelniam tablice..." << endl;
}

int main()
{
	compute(10);

	double dx = 3.15;
	compute(dx);

	//compute("324"); // error

	int tab1[1000];

	fill_buffer(tab1);

	system("PAUSE");
}