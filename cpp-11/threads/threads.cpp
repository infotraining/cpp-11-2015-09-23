#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <vector>
#include <mutex>
#include <algorithm>

using namespace std;

void background_task(int id, const string& text)
{
	static mutex cout_mtx;

	for(const auto& c : text)
	{
		unique_lock<mutex> lk{ cout_mtx };
		cout << "THD#: " << id << " - " << c << endl;
		lk.unlock();
		
	
		this_thread::sleep_for(chrono::milliseconds(500)); // 500ms
	}

	lock_guard<mutex> lk{ cout_mtx };
	cout << "END of THD#" << id << endl;
}

int main()
{
	//thread thd1{ [] { background_task(1, "Hello"); } };
	cout << "Hardware concurrency: " << thread::hardware_concurrency() << endl;

	vector<thread> threads;
	for (int id = 1; id <= max(thread::hardware_concurrency(), 1U); ++id)
		threads.emplace_back([id] { background_task(id, "Thread"); });

	background_task(0, "Main");

	for (auto& thd : threads)
		thd.join();

	cout << "END of Main thread" << endl;

	system("PAUSE");
}
