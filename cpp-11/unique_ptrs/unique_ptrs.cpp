#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <assert.h>

using namespace std;

class Data
{
	int data_;
	string name_;
public:
	Data(int data = -1, const string& name = "unknown") : data_{data}, name_{name}
	{
		cout << "Data(" << data_ << ", " << name_ << ")" << endl;
	}

	~Data()
	{
		cout << "~Data(" << data_ << ", " << name_ << ")" << endl;
	}

	int data() const
	{
		return data_;
	}

	string name() const
	{
		return name_;
	}
};

class Person
{
	unique_ptr<Data> data_;
	unique_ptr<Data[]> array_of_data_;
	const size_t SIZE = 10;
public:
	Person(int data, const string& name) : data_{ make_unique<Data>(data, name) }, array_of_data_{ new Data[SIZE] }
	{}

	void use() const
	{
		cout << "Using " << data_->name() << endl;
		for (size_t i = 0; i < SIZE; ++i)
			cout << " + " << array_of_data_[0].name() << endl;;
	}
};

int main()
{
	{
		unique_ptr<Data> ptr1 = make_unique<Data>(13, "Gadget1");  // new Data(13, "Gadget1");

		unique_ptr<Data> ptr2{ new Data(45, "Gadget2") };

		unique_ptr<Data> ptr3 = move(ptr1);

		vector<unique_ptr<Data>> vec;

		vec.push_back(move(ptr2));
		vec.push_back(move(ptr3));
		vec.push_back(make_unique<Data>(65, "Gadget3"));

		for (const auto& ptr : vec)
		{
			cout << ptr->name() << endl;
		}

		vec[0].reset();
		vec[1] = make_unique<Data>(102, "SuperGadget");

		assert(vec[0] == nullptr);

		cout << "End of scope" << endl;
	}
	
	cout << "\n\n";

	{
		Person p1{ 1, "Gdgt" };
		p1.use();

		Person p2 = move(p1);
		p2.use();
	}

	system("PAUSE");
}