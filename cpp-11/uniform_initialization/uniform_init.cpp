#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <map>

using namespace std;

struct Data
{
	int x, y;
	Data(int x, int y) : x(x), y(y) {}
};

double get_value()
{
	return 3.14;
}

class Point
{
	int x_, y_;

public:
	Point(int x, int y) : x_{ x }, y_{ y }
	{
	}

	int x() const
	{
		return x_;
	}

	int y() const
	{
		return y_;
	}
};

void print_items(initializer_list<int> lst)
{
	cout << "size: " << lst.size() << "; items: ";
	for (auto it = lst.begin(); it != lst.end(); ++it)
		cout << *it << " ";
	cout << endl;
}

using Vector = vector<int>;

template<typename T>
using Dict = map<string, T>;

int main()
{
	int x{ 5 };

	const int cval{ 100 };

	Point pt{ 1, 2 };

	cout << pt.x() << ", " << pt.y() << endl;

	vector<Point> pts = { Point{1, 6}, pt, {7, 3} };

	int value{ static_cast<int>(get_value()) };

	int tab[10] = { 1, 2, 3, 4 };
	array<int, 10> arr = { 1, 2, 3, 4, 5 };

	Vector vec = { 1, 2, 3, 4, 5 };

	print_items({ 1, 2, 3, 4, 5, 6 });

	vector<int> vec2{10, 1};

	for(const auto& item : vec2)
	{
		cout << item << " ";
	}
	cout << endl;

	Dict<int> words = { {"one", 1}, {"two", 2}, {"three", 3} }; // map<string, int>

	for(const auto& kv : words)
	{
		cout << kv.first << " - " << kv.second << endl;
	}

	system("PAUSE");
}