#include <iostream>
#include <string>
#include <array>

using namespace std;

constexpr int square(int x)
{
	return x * x;
}

constexpr size_t factorial(size_t n)
{
	return (n == 0) ? 1 : n * factorial(n - 1);
}

template <typename T, size_t N>
constexpr size_t size_of_array(T(&)[N])
{
	return N;
}

int main()
{
	constexpr int cx1 = 2;

	constexpr int cx2 = cx1 * 2;
	
	const int var1 = 12;

	constexpr auto cx3 = var1 * cx2;

	constexpr auto squared48 = square(48);

	int tab[factorial(4)] = {};

	static_assert(factorial(4) == 24, "Error");

	static_assert(size_of_array(tab) == 24, "Invalid size of array");

	array<int, factorial(8)> arr = {};

	static_assert(arr.size() == factorial(8), "Invalid size of array");
}